package com.cheetay.moviesapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.cheetay.moviesapp.data.FavoriteMovies
import com.cheetay.moviesapp.data.Movie

@Database(entities = [Movie::class , FavoriteMovies::class], version = 1)
abstract class MovieDatabase : RoomDatabase() {

  abstract fun movieDao(): MovieDao
}