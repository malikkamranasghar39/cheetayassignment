package com.cheetay.moviesapp.respository

import androidx.room.withTransaction
import com.cheetay.moviesapp.apiservice.FetchMoviesApiService
import com.cheetay.moviesapp.base.BaseRepository
import com.cheetay.moviesapp.data.FavoriteMovies
import com.cheetay.moviesapp.db.MovieDatabase
import com.cheetay.moviesapp.network.networkBoundResource
import javax.inject.Inject

class FetchMoviesListRepository @Inject constructor(
    private val fetchMoviesApiService: FetchMoviesApiService,
    private val movieDb: MovieDatabase
) : BaseRepository() {

    private val movieDao = movieDb.movieDao()

    fun getMovies(page: Int) = networkBoundResource(
        query = {
            movieDao.getAllMoviesList()
        },
        fetch = {
            fetchMoviesApiService.fetchMoviesList(page = page)
        },
        saveFetchResult = { movies ->
            movieDb.withTransaction {
                movieDao.deleteAllMovies()
                movieDao.addMovie(movies.movie)
            }
        },
    )

    suspend fun addMovieToFavorites(movie: FavoriteMovies) {
        movieDao.addMovieToFavorite(movie)
    }
}