package com.cheetay.moviesapp.respository

import com.cheetay.moviesapp.base.BaseRepository
import com.cheetay.moviesapp.data.FavoriteMovies
import com.cheetay.moviesapp.data.SearchMovieApiResponse
import com.cheetay.moviesapp.datasource.SearchMovieDataSource
import com.cheetay.moviesapp.db.MovieDatabase
import com.cheetay.moviesapp.network.Resource
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class SearchMovieRepository @Inject constructor(
  private val searchMovieDataSource: SearchMovieDataSource,
  movieDb: MovieDatabase
) : BaseRepository() {

  private val movieDao = movieDb.movieDao()

  suspend fun fetchSearchedMovie(page: Int , movieName: String): Flow<Resource<out SearchMovieApiResponse>> {
    return flow {
      emit(Resource.Loading(null))
      val result = searchMovieDataSource.fetchSearchedMovie(page = page , movieName = movieName)
      emit(result)
    }.flowOn(Dispatchers.IO)
  }

  suspend fun addMovieToFavorites(movie: FavoriteMovies) {
    movieDao.addMovieToFavorite(movie)
  }
}