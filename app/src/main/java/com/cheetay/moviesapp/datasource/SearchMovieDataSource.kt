package com.cheetay.moviesapp.datasource

import com.cheetay.moviesapp.apiservice.SearchMoviesApiService
import com.cheetay.moviesapp.base.BaseResponse
import javax.inject.Inject

class SearchMovieDataSource @Inject constructor
  (private val searchMoviesApiService: SearchMoviesApiService): BaseResponse() {

  suspend fun fetchSearchedMovie(page: Int , movieName: String) = getResult {
    searchMoviesApiService.searchMoviesList(page = page , query = movieName)
  }
}