package com.cheetay.moviesapp.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cheetay.moviesapp.respository.FavoritesMovieRepository
import com.cheetay.moviesapp.respository.FetchMoviesListRepository
import com.cheetay.moviesapp.respository.SearchMovieRepository
import com.cheetay.moviesapp.ui.favorite.FavoriteMoviesViewModel
import com.cheetay.moviesapp.ui.movieslist.MoviesListViewModel
import com.cheetay.moviesapp.ui.search.SearchMovieViewModel
import javax.inject.Inject

class ViewModelFactory @Inject constructor(private val repository: BaseRepository, private val paramters: Any ?= null) :
  ViewModelProvider.Factory {
  // add your view models here
  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    return when {
      //  example below
      modelClass.isAssignableFrom(MoviesListViewModel::class.java) -> MoviesListViewModel(repository as FetchMoviesListRepository) as T
      modelClass.isAssignableFrom(FavoriteMoviesViewModel::class.java) -> FavoriteMoviesViewModel(repository as FavoritesMovieRepository) as T
      modelClass.isAssignableFrom(SearchMovieViewModel::class.java) -> SearchMovieViewModel(repository as SearchMovieRepository) as T
      else -> throw IllegalAccessException("Unknown View Model.Please add your view model in factory")
    }
  }
}