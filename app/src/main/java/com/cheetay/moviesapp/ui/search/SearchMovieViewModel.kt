package com.cheetay.moviesapp.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cheetay.moviesapp.data.FavoriteMovies
import com.cheetay.moviesapp.data.SearchMovieApiResponse
import com.cheetay.moviesapp.network.Resource
import com.cheetay.moviesapp.respository.SearchMovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchMovieViewModel @Inject constructor(
  private val repository: SearchMovieRepository
) : ViewModel() {

  private val _searchMovieApiResponse = MutableLiveData<Resource<out SearchMovieApiResponse>>()
  val searchMovieApiResponse = _searchMovieApiResponse

  fun fetchSearchedMovie(page: Int , movieName: String) {
    viewModelScope.launch {
      repository.fetchSearchedMovie(page , movieName).collect {
        _searchMovieApiResponse.value = it
      }
    }
  }

  fun addMovieToFavorites(movie: FavoriteMovies) {
    viewModelScope.launch {
      repository.addMovieToFavorites(movie)
    }
  }
}