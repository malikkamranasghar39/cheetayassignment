package com.cheetay.moviesapp.ui.favorite

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cheetay.moviesapp.data.Movie
import com.cheetay.moviesapp.respository.FavoritesMovieRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteMoviesViewModel @Inject constructor(
  private val repository: FavoritesMovieRepository
) : ViewModel() {

  private val _favoriteMovieList = MutableLiveData<List<Movie>>()
  val favoriteMovieList = _favoriteMovieList

  fun fetchFavoriteMovies() {
    viewModelScope.launch {
      repository.fetchFavoriteMovies().collect {
        _favoriteMovieList.value = it
      }
    }
  }

  fun removeFavoriteMovie(movieId: Int) {
    viewModelScope.launch {
      repository.removeMovieFromFavorite(movieId)
    }
  }
}