package com.cheetay.moviesapp.ui.movieslist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cheetay.moviesapp.data.FavoriteMovies
import com.cheetay.moviesapp.data.Movie
import com.cheetay.moviesapp.network.Resource
import com.cheetay.moviesapp.respository.FetchMoviesListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesListViewModel @Inject constructor(private val repository: FetchMoviesListRepository) : ViewModel() {

  private val _moviesList =  MutableLiveData<Resource<List<Movie>>>()
  val moviesList = _moviesList


  fun fetchMoviesList(page: Int) {
    viewModelScope.launch {
      repository.getMovies(page).collect {
        _moviesList.value = it
      }
    }
  }

  fun addMovieToFavorites(movie: FavoriteMovies) {
    viewModelScope.launch {
      repository.addMovieToFavorites(movie)
    }
  }
}